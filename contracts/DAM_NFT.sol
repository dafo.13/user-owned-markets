// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721EnumerableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/StringsUpgradeable.sol";


contract DAM0 is Initializable, ERC721Upgradeable, ERC721EnumerableUpgradeable, OwnableUpgradeable, UUPSUpgradeable {
    using CountersUpgradeable for CountersUpgradeable.Counter;

    CountersUpgradeable.Counter private _tokenIdCounter;


    string public baseURI;
    uint public price;

    function initialize(string memory name, string memory symbol, string memory URI, address owners, uint _price) initializer public {
        __ERC721_init(name, symbol);//"Assayer", "DAM0"
        __ERC721Enumerable_init();
        __Ownable_init();
        __UUPSUpgradeable_init();

        baseURI=URI;//"https://dam.albin.si/json/"
        _transferOwnership(owners);
        price=_price;
    }

    function _baseURI() internal view override returns (string memory) {
        return baseURI;
    }

    function mint(uint number) public payable {
        uint _price;
		for (uint i; i < number; i++) 
			_price += price*2**i;
        require(msg.value >= _price, "Not enough ETH");

        (bool sent, bytes memory data) = owner().call{value: msg.value}("");
        require(sent, "Failed to send Ether");

		for (uint i; i<number; i++) {
			_safeMint(msg.sender, _tokenIdCounter.current());
			_tokenIdCounter.increment();
            price*=2;
		}
    }

    function _authorizeUpgrade(address newImplementation)
        internal
        onlyOwner
        override
    {}

    // The following functions are overrides required by Solidity.

    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        override(ERC721Upgradeable, ERC721EnumerableUpgradeable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721Upgradeable, ERC721EnumerableUpgradeable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        string memory _baseURI = _baseURI();
        return string(abi.encodePacked(baseURI, StringsUpgradeable.toString(tokenId%10)));
    }

    function updateBase(string memory newBaseURI) external onlyOwner {
      baseURI=newBaseURI;
    }
}