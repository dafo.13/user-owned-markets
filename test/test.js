// ethers <=5.5.0  npm install --save-dev @nomiclabs/hardhat-ethers 'ethers@^5.4.7'
const { ethers, upgrades } = require("hardhat");
const { expect } = require("chai");


describe("DAM testing", function () {
	const baseprice = 1000000000000000;
	
	beforeEach(async function () {
		const [deployer, buyer, seller, paymentTokenAddress, assayer1, assayer2, assayer3, multisig, delegate, multisig2, counter] = await ethers.getSigners();
		this.deployer = deployer;
		this.buyer = buyer;
		this.seller = seller;
		this.counter = counter;
		this.paymentTokenAddress = paymentTokenAddress;
		this.assayer1 = assayer1;
		this.assayer2 = assayer2;
		this.assayer3 = assayer3;
		this.multisig = multisig;
		this.delegate = delegate;
		this.multisig2 = multisig2;
		
		const TestToken = await ethers.getContractFactory("TestToken");
		
		const erc20 = await TestToken.deploy("TestToken","TTT");
		this.erc20 = erc20;
		
		await erc20.transfer(buyer.address, baseprice);
		
		await erc20.transfer(counter.address, baseprice/2);//giving seller 1/2 of funds for "counter" testing
		
		await erc20.transfer(assayer3.address, baseprice);
		
		let DAMt = await ethers.getContractFactory("DAM_NFT");

		let ECDSA = await ethers.getContractFactory("ECDSA_min");
		
		let DAM = await ethers.getContractFactory("DAM_Escrow");

		this.DAMt = await DAMt.deploy();
		await this.DAMt.deployed();		

		this.ECDSA = await ECDSA.deploy();
		await this.ECDSA.deployed();		
		
		this.DAM = await upgrades.deployProxy(DAM,[this.multisig.address, this.DAMt.address,100,0, this.ECDSA.address],{ initializer: 'initialize', kind: 'uups', });
		await this.DAM.deployed(); 
		
	});
 	it("Transfer ownership.", async function () {
		await this.DAM.connect(this.multisig).transferOwnership(this.multisig2.address,0);
		expect((await this.DAM.getMarket(0))[0]).to.equal(this.multisig2.address);
	});
	
 	it("Minting 10 NFT tokens to multisig.", async function () {
		await this.DAMt.connect(this.multisig).mint(10);
		expect(await this.DAMt.balanceOf(this.multisig.address)).to.equal(10);
	});
	
	it("Asigning delegate/minion and banning assayers NFT.", async function () {
		await this.DAMt.connect(this.assayer1).mint(1);
		await this.DAMt.connect(this.assayer2).mint(1);
		await this.DAM.connect(this.multisig).assignDelegate(this.delegate.address,0);
		
		await this.DAM.connect(this.delegate).banTokenID([0],0);

		expect(await this.DAM.banned([0],[0])).to.equal(true);//banned
		expect(await this.DAM.banned([0],[1])).to.equal(false);//not banned
	});
	
	it("Banning assayers NFT by multisig.", async function () {
		await this.DAMt.connect(this.assayer1).mint(1);
		await this.DAMt.connect(this.assayer2).mint(1);
		
		await this.DAM.connect(this.multisig).banTokenID([0],0);

		expect(await this.DAM.banned([0],[0])).to.equal(true);//banned
		expect(await this.DAM.banned([0],[1])).to.equal(false);//not banned
	});
	
	it("Transaction resolved without audit.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 0 blocks, meaning the transaction gets immediately resolved allowing the seller to withdraw the money
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		["0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address,  baseprice, 0]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));

		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, this.seller.address, "0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address, baseprice, 0, signedByBuyer, signedBySeler, 0);
		await this.DAM.connect(this.seller).withdraw(0);
		expect(await this.erc20.balanceOf(this.seller.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
	});
	
	it("Transaction resolved without audit + seller changes address", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 0 blocks, meaning the transaction gets immediately resolved allowing the seller to withdraw the money
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		["0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address,  baseprice, 0]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));

		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, this.seller.address, "0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address, baseprice, 0, signedByBuyer, signedBySeler, 0);
		await this.DAM.connect(this.seller).swapAddress(0,this.delegate.address)//using 'delegate' as swap address
		await this.DAM.connect(this.delegate).withdraw(0);
		expect(await this.erc20.balanceOf(this.delegate.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
	});
	
 	it("Transaction resolved without audit + transaction initialized by the market owner.", async function () {
		await this.erc20.transfer(this.multisig.address, baseprice);
		await this.erc20.connect(this.multisig).approve(this.DAM.address,baseprice);
		//time set as 0 blocks, meaning the transaction gets immediately resolved allowing the seller to withdraw the money
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		["0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address,  baseprice, 0]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));

		await this.DAM.connect(this.multisig).createTransaction(this.multisig.address, this.seller.address, "0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address, baseprice, 0, "0x00", signedBySeler, 0);
		await this.DAM.connect(this.seller).withdraw(0);
		expect(await this.erc20.balanceOf(this.seller.address)).to.equal(baseprice);//without fee
	});
	
 	it("Transaction resolved with audit and assayers agree in favor of seller.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 2 blocks
		
		const hash="0x616272616b616461627261000000000000000000000000000000000000000000";
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		[hash, this.erc20.address,  baseprice, 7]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));
		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, this.seller.address, hash, this.erc20.address, baseprice, 7, signedByBuyer, signedBySeler, 0);
		
		//using assayer3 as person who will call audit
		await this.erc20.connect(this.assayer3).approve(this.DAM.address,baseprice);
		await this.DAM.connect(this.assayer3).audit(0,7,this.erc20.address, baseprice);
		
		await this.DAMt.connect(this.assayer1).mint(1);
		await this.DAMt.connect(this.assayer2).mint(1);
		
		const saltedHash = await this.DAM.getSaltedHash(false, hash);
		
		await this.DAM.connect(this.assayer1).commitAssay(0,saltedHash,this.DAMt.address,0);
		await this.DAM.connect(this.assayer2).commitAssay(0,saltedHash,this.DAMt.address,1);
		
		await this.DAM.connect(this.assayer1).revealAssay(0,false,hash,0);
		await this.DAM.connect(this.assayer2).revealAssay(0,false,hash,1);
		
		//----------filler line to wait one block, since time enforcement does not count in "equal time" its either less than current block or more than current block
		await this.DAMt.connect(this.multisig).mint(1);
		
		await this.DAM.connect(this.seller).withdraw(0);
		expect(await this.erc20.balanceOf(this.seller.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
	});
	
	
	it("Transaction resolved with audit and assayers agree in favor of buyer.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 2 blocks
		
		const hash="0x616272616b616461627261000000000000000000000000000000000000000000";
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		[hash, this.erc20.address,  baseprice, 7]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));
		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, this.seller.address, hash, this.erc20.address, baseprice, 7, signedByBuyer, signedBySeler,0);
		
		//using assayer3 as person who will call audit
		await this.erc20.connect(this.assayer3).approve(this.DAM.address,baseprice);
		await this.DAM.connect(this.assayer3).audit(0,7,this.erc20.address, baseprice);
		
		await this.DAMt.connect(this.assayer1).mint(1);
		await this.DAMt.connect(this.assayer2).mint(1);
		
		const saltedHash = await this.DAM.getSaltedHash(true, hash);
		
		await this.DAM.connect(this.assayer1).commitAssay(0,saltedHash,this.DAMt.address,0);
		await this.DAM.connect(this.assayer2).commitAssay(0,saltedHash,this.DAMt.address,1);
		
		await this.DAM.connect(this.assayer1).revealAssay(0,true,hash,0);
		await this.DAM.connect(this.assayer2).revealAssay(0,true,hash,1);
		
		//----------filler line to wait one block, since time enforcement does not count in "equal time" its either less than current block or more than current block
		await this.DAMt.connect(this.multisig).mint(1);
		
		await this.DAM.connect(this.buyer).withdraw(0);
		expect(await this.erc20.balanceOf(this.buyer.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
	});
	
	it("Transaction resolved with decision from multisig in favor of seller.", async function () {

		//await this.erc20.balanceOf(this.assayer3.address).then(val => console.log(val));

		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		const hash="0x616272616b616461627261000000000000000000000000000000000000000000";
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		[hash, this.erc20.address,  baseprice, 3]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));
		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, this.seller.address, hash, this.erc20.address, baseprice, 3, signedByBuyer, signedBySeler,0);
		
		await this.erc20.connect(this.assayer3).approve(this.DAM.address,baseprice);
		await this.DAM.connect(this.assayer3).audit(0,9,this.erc20.address, baseprice);
		
		await this.DAM.connect(this.multisig).resolveAudit(0,false);
		
		//audit resolve by multisig
		await this.DAM.connect(this.seller).withdraw(0);

		expect(await this.erc20.balanceOf(this.seller.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
		
	});
	
	it("Transaction resolved with decision from multisig in favor of buyer.", async function () {

		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		const hash="0x616272616b616461627261000000000000000000000000000000000000000000";
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		[hash, this.erc20.address,  baseprice, 3]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));
		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, this.seller.address, hash, this.erc20.address, baseprice, 3, signedByBuyer, signedBySeler,0);
		
		await this.erc20.connect(this.assayer3).approve(this.DAM.address,baseprice);
		await this.DAM.connect(this.assayer3).audit(0,9,this.erc20.address, baseprice);
		
		await this.DAM.connect(this.multisig).resolveAudit(0,true);
		
		//audit resolve by multisig
		await this.DAM.connect(this.buyer).withdraw(0);

		expect(await this.erc20.balanceOf(this.buyer.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
		
	});
 
 	it("Transaction resolved with audit and assayers agree in favor of seller + assayer from other DAM.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 2 blocks
		
		const hash="0x616272616b616461627261000000000000000000000000000000000000000000";
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		[hash, this.erc20.address,  baseprice, 7]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));
		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, this.seller.address, hash, this.erc20.address, baseprice, 7, signedByBuyer, signedBySeler,0);
		
		//setting up external assayer 
		await this.DAM.connect(this.multisig2).createMarket(this.multisig2.address,this.DAMt.address,100,5760);
		await this.DAMt.connect(this.multisig2).mint(1);
		await this.DAM.connect(this.multisig).addOtherMarket(1,0);
		//using assayer3 as person who will call audit
		await this.erc20.connect(this.assayer3).approve(this.DAM.address,baseprice);
		await this.DAM.connect(this.assayer3).audit(0,9,this.erc20.address, baseprice);
		
		await this.DAMt.connect(this.assayer1).mint(1);
		await this.DAMt.connect(this.assayer2).mint(1);
		
		const saltedHash = await this.DAM.getSaltedHash(false, hash);
		
		await this.DAM.connect(this.assayer1).commitAssay(0,saltedHash,this.DAMt.address,1);
		await this.DAM.connect(this.assayer2).commitAssay(0,saltedHash,this.DAMt.address,2);
		await this.DAM.connect(this.multisig2).commitAssay(0,saltedHash,this.DAMt.address,0);
		
		await this.DAM.connect(this.assayer1).revealAssay(0,false,hash,0);
		await this.DAM.connect(this.assayer2).revealAssay(0,false,hash,1);
		await this.DAM.connect(this.multisig2).revealAssay(0,false,hash,2);
		
		//----------filler line to wait one block, since time enforcement does not count in "equal time" its either less than current block or more than current block
		await this.DAMt.connect(this.multisig).mint(1);
		
		await this.DAM.connect(this.seller).withdraw(0);
		expect(await this.erc20.balanceOf(this.seller.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
	});

 	it("Transaction resolved with audit and assayers agree in favor of buyer + assayers from other DAM.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 2 blocks
		
		const hash="0x616272616b616461627261000000000000000000000000000000000000000000";
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		[hash, this.erc20.address,  baseprice, 7]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));
		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, this.seller.address, hash, this.erc20.address, baseprice, 7, signedByBuyer, signedBySeler,0);
		
		//setting up external assayer 
		await this.DAM.connect(this.multisig2).createMarket(this.multisig2.address,this.DAMt.address,100,5760);
		await this.DAMt.connect(this.multisig2).mint(2);
		await this.DAM.connect(this.multisig).addOtherMarket(1,0);
		//using assayer3 as person who will call audit
		await this.erc20.connect(this.assayer3).approve(this.DAM.address,baseprice);
		await this.DAM.connect(this.assayer3).audit(0,11,this.erc20.address, baseprice);
		
		await this.DAMt.connect(this.assayer1).mint(1);
		await this.DAMt.connect(this.assayer2).mint(1);

		const saltedHash = await this.DAM.getSaltedHash(true, hash);
		await this.DAM.connect(this.assayer1).commitAssay(0,saltedHash,this.DAMt.address,2);
		await this.DAM.connect(this.assayer2).commitAssay(0,saltedHash,this.DAMt.address,3);
		await this.DAM.connect(this.multisig2).commitAssay(0,saltedHash,this.DAMt.address,0);
		await this.DAM.connect(this.multisig2).commitAssay(0,saltedHash,this.DAMt.address,1);
		
		await this.DAM.connect(this.assayer1).revealAssay(0,true,hash,0);
		await this.DAM.connect(this.assayer2).revealAssay(0,true,hash,1);
		await this.DAM.connect(this.multisig2).revealAssay(0,true,hash,2);
		await this.DAM.connect(this.multisig2).revealAssay(0,true,hash,3);
		//----------filler line to wait one block, since time enforcement does not count in "equal time" its either less than current block or more than current block
		await this.DAMt.connect(this.multisig).mint(1);
		
		await this.DAM.connect(this.buyer).withdraw(0);
		expect(await this.erc20.balanceOf(this.buyer.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
	});
	
 	it("Transaction resolved with audit and assayers agree in favor of buyer + assayers from other DAM + assayers withdraw funds.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 2 blocks
		
		const hash="0x616272616b616461627261000000000000000000000000000000000000000000";
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		[hash, this.erc20.address,  baseprice, 7]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));
		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, this.seller.address, hash, this.erc20.address, baseprice, 7, signedByBuyer, signedBySeler,0);
		
		//setting up external assayer 
		await this.DAM.connect(this.multisig2).createMarket(this.multisig2.address,this.DAMt.address,100,5760);
		await this.DAMt.connect(this.multisig2).mint(2);
		await this.DAM.connect(this.multisig).addOtherMarket(1,0);
		//using assayer3 as person who will call audit
		await this.erc20.connect(this.assayer3).approve(this.DAM.address,baseprice);
		await this.DAM.connect(this.assayer3).audit(0,11,this.erc20.address, baseprice);
		
		await this.DAMt.connect(this.assayer1).mint(1);
		await this.DAMt.connect(this.assayer2).mint(1);

		const saltedHash = await this.DAM.getSaltedHash(true, hash);
		await this.DAM.connect(this.assayer1).commitAssay(0,saltedHash,this.DAMt.address,2);
		await this.DAM.connect(this.assayer2).commitAssay(0,saltedHash,this.DAMt.address,3);
		await this.DAM.connect(this.multisig2).commitAssay(0,saltedHash,this.DAMt.address,0);
		await this.DAM.connect(this.multisig2).commitAssay(0,saltedHash,this.DAMt.address,1);
		
		await this.DAM.connect(this.assayer1).revealAssay(0,true,hash,0);
		await this.DAM.connect(this.assayer2).revealAssay(0,true,hash,1);
		await this.DAM.connect(this.multisig2).revealAssay(0,true,hash,2);
		await this.DAM.connect(this.multisig2).revealAssay(0,true,hash,3);
		//----------filler line to wait one block, since time enforcement does not count in "equal time" its either less than current block or more than current block
		await this.DAMt.connect(this.multisig).mint(1);
		
		await this.DAM.connect(this.buyer).withdraw(0);
		
		await this.DAM.connect(this.assayer1).assayerWithdrawFee(0,0);
		await this.DAM.connect(this.assayer2).assayerWithdrawFee(0,1);
		await this.DAM.connect(this.multisig2).assayerWithdrawFees([[0,2],[0,3]]);
		
		expect(await this.erc20.balanceOf(this.buyer.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
		expect(await this.erc20.balanceOf(this.assayer1.address)).to.equal(baseprice*0.25);
		expect(await this.erc20.balanceOf(this.assayer2.address)).to.equal(baseprice*0.25);
		expect(await this.erc20.balanceOf(this.multisig2.address)).to.equal(baseprice*0.5);
	});

 	it("Transaction resolved with audit and assayers agree in favor of seller but multisig decides in the favor of buyer.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 2 blocks
		
		const hash="0x616272616b616461627261000000000000000000000000000000000000000000";
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		[hash, this.erc20.address,  baseprice, 7]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));
		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, this.seller.address, hash, this.erc20.address, baseprice, 7, signedByBuyer, signedBySeler,0);
		
		//using assayer3 as person who will call audit
		await this.erc20.connect(this.assayer3).approve(this.DAM.address,baseprice);
		await this.DAM.connect(this.assayer3).audit(0,8,this.erc20.address, baseprice);
		
		await this.DAMt.connect(this.assayer1).mint(1);
		await this.DAMt.connect(this.assayer2).mint(1);
		
		const saltedHash = await this.DAM.getSaltedHash(false, hash);
		
		await this.DAM.connect(this.assayer1).commitAssay(0,saltedHash,this.DAMt.address,0);
		await this.DAM.connect(this.assayer2).commitAssay(0,saltedHash,this.DAMt.address,1);
		
		await this.DAM.connect(this.assayer1).revealAssay(0,false,hash,0);
		await this.DAM.connect(this.assayer2).revealAssay(0,false,hash,1);
		
		await this.DAM.connect(this.multisig).resolveAudit(0,true);
		//----------filler line to wait one block, since time enforcement does not count in "equal time" its either less than current block or more than current block
		await this.DAMt.connect(this.multisig).mint(1);
		
		await this.DAM.connect(this.buyer).withdraw(0);
		expect(await this.erc20.balanceOf(this.buyer.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
	});
	
	
	it("Transaction resolved with audit and assayers agree in favor of buyer but multisig decides in the favor of seller.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 2 blocks
		
		const hash="0x616272616b616461627261000000000000000000000000000000000000000000";
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		[hash, this.erc20.address,  baseprice, 7]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));
		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, this.seller.address, hash, this.erc20.address, baseprice, 7, signedByBuyer, signedBySeler,0);
		
		//using assayer3 as person who will call audit
		await this.erc20.connect(this.assayer3).approve(this.DAM.address,baseprice);
		await this.DAM.connect(this.assayer3).audit(0,8,this.erc20.address, baseprice);
		
		await this.DAMt.connect(this.assayer1).mint(1);
		await this.DAMt.connect(this.assayer2).mint(1);
		
		const saltedHash = await this.DAM.getSaltedHash(true, hash);
		
		await this.DAM.connect(this.assayer1).commitAssay(0,saltedHash,this.DAMt.address,0);
		await this.DAM.connect(this.assayer2).commitAssay(0,saltedHash,this.DAMt.address,1);
		
		await this.DAM.connect(this.assayer1).revealAssay(0,true,hash,0);
		await this.DAM.connect(this.assayer2).revealAssay(0,true,hash,1);
		
		await this.DAM.connect(this.multisig).resolveAudit(0,false);
		
		//----------filler line to wait one block, since time enforcement does not count in "equal time" its either less than current block or more than current block
		await this.DAMt.connect(this.multisig).mint(10);
		
		await this.DAM.connect(this.seller).withdraw(0);
		expect(await this.erc20.balanceOf(this.seller.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
	});
	
 	it("Create 'new market' and confirm owner.", async function () {
		await this.DAM.connect(this.multisig2).createMarket(this.multisig2.address,this.DAMt.address,100,5760);
		expect((await this.DAM.getMarket(1))[0]).to.equal(this.multisig2.address);
	});
	
	it("Transaction resolved with non-specified seller.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 0 blocks, meaning the transaction gets immediately resolved allowing the seller to withdraw the money
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		["0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address,  baseprice, 0]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));
		

		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, "0x0000000000000000000000000000000000000000", "0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address, baseprice, 0, signedByBuyer, "0x00", 0);
		await this.DAM.connect(this.seller).takeOpenTransaction(0,signedBySeler,"0x616272616b616461627261000000000000000000000000000000000000000123",baseprice,0);
		await this.DAM.connect(this.buyer).confirmTransaction(1);
		await this.DAM.connect(this.seller).withdraw(1);
		expect(await this.erc20.balanceOf(this.seller.address)).to.equal(baseprice*0.99);//hardcoded 1% fee to multisig
	});
	
	it("Transaction resolved with non-specified seller | counter - buyer takes funds.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		await this.erc20.connect(this.counter).approve(this.DAM.address,baseprice/2);
		//time set as 0 blocks, meaning the transaction gets immediately resolved allowing the seller to withdraw the money
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["uint256"],
		[baseprice/2]));//expecting counter to join in with 1/2 of funds
		//let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));

		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, "0x0000000000000000000000000000000000000000", "0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address, baseprice, 0, signedByBuyer, "0x00", 0);
		await this.DAM.connect(this.counter).takeOpenTransaction(0,signedByBuyer,"0x616272616b616461627261000000000000000000000000000000000000000000",0,baseprice/2);
		await this.DAM.connect(this.buyer).withdraw(0);
		expect(await this.erc20.balanceOf(this.buyer.address)).to.equal(baseprice*1.5*0.99);//hardcoded 1% fee to multisig
	});
	
	it("Transaction resolved with non-specified seller | counter - seller takes funds.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		await this.erc20.connect(this.counter).approve(this.DAM.address,baseprice/2);
		//time set as 0 blocks, meaning the transaction gets immediately resolved allowing the seller to withdraw the money
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["uint256"],
		[baseprice/2]));//expecting counter to join in with 1/2 of funds
		//let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));

		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, "0x0000000000000000000000000000000000000000", "0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address, baseprice, 0, signedByBuyer, "0x00", 0);
		await this.DAM.connect(this.counter).takeOpenTransaction(0,signedByBuyer,"0x616272616b616461627261000000000000000000000000000000000000000000",0,baseprice/2);
		await this.DAM.connect(this.counter).withdraw(0);
		expect(await this.erc20.balanceOf(this.counter.address)).to.equal(baseprice*1.5*0.99);//hardcoded 1% fee to multisig
	});
	
	it("Transaction canceled by creator.", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 0 blocks, meaning the transaction gets immediately resolved allowing the seller to withdraw the money
		let parameters = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		["0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address,  baseprice, 0]));
		//let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parameters));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parameters));

		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, "0x0000000000000000000000000000000000000000", "0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address, baseprice, 0, signedByBuyer, "0x00", 0);
		await this.DAM.connect(this.buyer).withdraw(0);
		expect(await this.erc20.balanceOf(this.buyer.address)).to.equal(baseprice);
	});
	
	it("Partial order", async function () {
		await this.erc20.connect(this.buyer).approve(this.DAM.address,baseprice);
		//time set as 0 blocks, meaning the transaction gets immediately resolved allowing the seller to withdraw the money
		let parametersBuyer = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		["0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address,  baseprice, 0]));
		let parametersSeller = ethers.utils.keccak256(ethers.utils.solidityPack(
		["bytes32", "address", "uint256", "uint256"],
		["0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address,  baseprice*0.5, 0]));
		let signedBySeler = await this.seller.signMessage(ethers.utils.arrayify(parametersSeller));
		let signedByBuyer = await this.buyer.signMessage(ethers.utils.arrayify(parametersBuyer));

		await this.DAM.connect(this.buyer).createTransaction(this.buyer.address, "0x0000000000000000000000000000000000000000", "0x616272616b616461627261000000000000000000000000000000000000000000", this.erc20.address, baseprice, 0, signedByBuyer, "0x00", 0);
		await this.DAM.connect(this.seller).takeOpenTransaction(0,signedBySeler,"0x616272616b616461627261000000000000000000000000000000000000000123",baseprice*0.5,0);
		await this.DAM.connect(this.buyer).confirmTransaction(1);
		await this.DAM.connect(this.seller).withdraw(1);
		expect(await this.erc20.balanceOf(this.seller.address)).to.equal((baseprice*0.5)*0.99);//hardcoded 1% fee to multisig
		expect((await this.DAM.getTransaction(0))[4]).to.equal(baseprice*0.5);// confirming that paymentAmount from inital transaction got reduced 
	});

});
